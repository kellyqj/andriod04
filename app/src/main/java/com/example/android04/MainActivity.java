package com.example.android04;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button createA, delete, rename, search;

    EditText createAnAlbum, renameAlbum, originalName, deleteAlbum;

    ListView albums;

    static ArrayList<Album> obvAlbums = new ArrayList<Album>();

    ArrayAdapter<Album> adapter;

    int in = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        search = (Button)findViewById(R.id.search);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, search_photos.class);
                intent.putExtra("albums", obvAlbums);
                startActivity(intent);
            }
        });

        createA = (Button)findViewById(R.id.createA);
        albums = (ListView)findViewById(R.id.albums);
        adapter = new ArrayAdapter<Album>(MainActivity.this, android.R.layout.simple_list_item_1, obvAlbums);
        albums.setAdapter(adapter);
        createA.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createAlbum(adapter);
            }

        });
        albums.setOnItemClickListener((parent, view, position, id) -> showAlbum(position));

        rename = (Button)findViewById(R.id.rename);
        rename.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                renameAlbum(adapter);
            }

        });

        delete = (Button)findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteAlbum(adapter);
            }

        });
    }

    private void showAlbum(int pos) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("album", obvAlbums.get(pos));
        Intent intent = new Intent(this, album_details.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void createAlbum(ArrayAdapter adapter) {
        createAnAlbum = (EditText) findViewById(R.id.createAnAlbum);
        String name = createAnAlbum.getText().toString();

        Album addAlbum = new Album();
        addAlbum.setName(name);

       if(name.trim().equals("")) { //nothing in the textbox
           new AlertDialog.Builder(MainActivity.this)
                   .setTitle("Empty album name")
                   .setMessage("There is no name entered")
                   .setNegativeButton(android.R.string.no, null)
                   .setIcon(android.R.drawable.ic_dialog_alert)
                   .show();
           return;
       }

        for(int i = 0; i < obvAlbums.size(); i++) { //loops into obvSongs to check if the album exists or not
            if(obvAlbums.get(i).getName().equalsIgnoreCase(addAlbum.getName())) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Album already exists")
                        .setMessage("This album already exists!")
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return;
            }
        }

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Add Album")
                .setMessage("Are you sure you want to add this album?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        obvAlbums.add(addAlbum);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void renameAlbum(ArrayAdapter<Album> adapter){
        originalName = (EditText) findViewById(R.id.originalName);
        String name = originalName.getText().toString();

        renameAlbum = (EditText) findViewById(R.id.renameAlbum);
        String alRename = renameAlbum.getText().toString();

        if(name.trim().equals("") || alRename.trim().equals("")) { //nothing in the textbox
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Empty album name")
                    .setMessage("There is no name entered")
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        for(int i = 0; i < obvAlbums.size(); i++) { //loops into obvSongs to check if the album exists or not
            if(obvAlbums.get(i).getName().equalsIgnoreCase(alRename)) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Album already exists")
                        .setMessage("This album already exists!")
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return;
            }
        }

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Rename Album")
                .setMessage("Are you sure you want to rename this album?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        for(int i = 0; i < obvAlbums.size(); i++) { //loops into obvSongs to check if the album exists or not
                            if(obvAlbums.get(i).getName().equalsIgnoreCase(name)) {
                                obvAlbums.get(i).setName(alRename);
                                adapter.notifyDataSetChanged();
                                break;
                            }
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

    public void deleteAlbum(ArrayAdapter<Album> adapter) {
        deleteAlbum = (EditText)findViewById(R.id.deleteAlbum);
        String deleteAl = deleteAlbum.getText().toString();

        if(deleteAl.trim().equals("")) { //nothing in the textbox
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Empty album name")
                    .setMessage("There is no name entered")
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }


        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Delete Album")
                .setMessage("Are you sure you want to delete this album?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean found = false;
                        for(int i = 0; i < obvAlbums.size(); i++) { //loops into obvSongs to check if the album exists or not
                            if(obvAlbums.get(i).getName().equalsIgnoreCase(deleteAl)) { //obvAlbum name = delete name
                                found = true;
                                in = i;
                                break;
                            }
                        }
                        if(found) {
                            obvAlbums.remove(in);
                            adapter.notifyDataSetChanged();
                        } else { //found is false
                            new AlertDialog.Builder(MainActivity.this)
                                    .setTitle("Album not found")
                                    .setMessage("Album cannot be found!")
                                    .setNegativeButton(android.R.string.no, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                            return;
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }


    /*
    public void deleteAlbum(ActionEvent e) {
        //confirmation box
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Delete Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete this song?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Album album = albums.getSelectionModel().getSelectedItem();
            int index = obvAlbums.indexOf(album);
            obvAlbums.remove(album);
            user.albums.remove(index);
        }
    }

    public void serialize(String filename) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(User.file));
        oos.writeObject(user);
        for(int i = 0; i < user.albums.size(); i++) {
            oos.writeObject(user.albums.get(i));
        }
    }

    public User deserialize() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(User.file));
        User readUser = (User)ois.readObject();
        System.out.println("deserialized");
        return readUser;
    }

    public void logout(ActionEvent e) throws IOException {
        //serialize(User.file);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/login.fxml"));
        AnchorPane pane = (AnchorPane)loader.load();
        rootPane.getChildren().setAll(pane);
        PhotosController listController = loader.getController();
        listController.start(pane);
    }*/
}