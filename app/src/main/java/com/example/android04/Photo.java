package com.example.android04;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.net.URI;
import java.util.ArrayList;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Photo implements Serializable{

    ArrayList<Tag> tags = new ArrayList<Tag>();
    Uri uri;


    public Photo(Uri imageURI) {
        uri = imageURI;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public Tag getTag(int index) {
        return tags.get(index);
    }

    public Uri getUri() {
        return uri;
    }

    public String toString() {
        String str = uri.toString();

        return str;

    }

}
