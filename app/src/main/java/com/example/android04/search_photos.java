package com.example.android04;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class search_photos extends AppCompatActivity {

    String item1;
    String item2;

    ArrayList<Album> albums;

    EditText tagValue, tagValue2;

    Button and, or;

    public static GridView gridView;
    public static MyAdapter2 imgAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_photos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //gets information for albums
        albums = (ArrayList<Album>) getIntent().getSerializableExtra("albums");

        gridView = (GridView) findViewById(R.id.hello);

        //setting up the spinner1
        Spinner spinner1 = (Spinner)findViewById(R.id.tagType1);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                item1 = parent.getItemAtPosition(position).toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> tagTypes1 = new ArrayList<String>();
        tagTypes1.add("person");
        tagTypes1.add("location");

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(search_photos.this, android.R.layout.simple_spinner_item, tagTypes1);

        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(dataAdapter1);

        //setting up the spinner2
        Spinner spinner2 = (Spinner)findViewById(R.id.tagType2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                item2 = parent.getItemAtPosition(position).toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> tagTypes2 = new ArrayList<String>();
        tagTypes2.add("person");
        tagTypes2.add("location");

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(search_photos.this, android.R.layout.simple_spinner_item, tagTypes2);

        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner2.setAdapter(dataAdapter2);

        imgAdapter = new MyAdapter2(this);
        final GridView gridview = (GridView) findViewById(R.id.hello);
        gridview.setAdapter(imgAdapter);

        //photo list stuff, will figure this out with Dani

        and = (Button)findViewById(R.id.and);
        and.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                and();
            }

        });

        or = (Button)findViewById(R.id.or);
        or.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                or();
            }

        });


    }

    public void and() {
        tagValue = (EditText) findViewById(R.id.tagValue);
        String t1 = tagValue.getText().toString();
        tagValue2 = (EditText) findViewById(R.id.tageValue2);
        String t2 = tagValue2.getText().toString();

        boolean firstTagFound = false;
        boolean secondTagFound = false;
        for(int i = 0; i < albums.size(); i++) { //go through list of albums
            for (int j = 0; j < albums.get(i).photoPath.size(); j++) { //go through list of photos in album i
                for (int k = 0; k < albums.get(i).photoPath.get(j).tags.size(); k++) { //go through list of tags in photo j of album i
                    if ((albums.get(i).photoPath.get(j).tags.get(k).getTagType().equals(item1)
                            && albums.get(i).photoPath.get(j).tags.get(k).getTagValue().contains(t1))) {
                        firstTagFound = true;
                    }

                    if ((albums.get(i).photoPath.get(j).tags.get(k).getTagType().equals(item2)
                            && albums.get(i).photoPath.get(j).tags.get(k).getTagValue().contains(t2))) {
                        secondTagFound = true;
                    }
                }
                if (firstTagFound && secondTagFound) {
                    Uri imageUri = albums.get(i).photoPath.get(j).getUri();
                    imgAdapter.add(imageUri);
                    gridView.setAdapter(imgAdapter);
                    imgAdapter.notifyDataSetChanged();
                    /*InputStream stream = new FileInputStream(storeAlbums.get(i).photoPath.get(j).getPath());
                    Image list = new Image(stream);
                    ImageView imageView = new ImageView();
                    imageView.setImage(list);
                    imageView.setX(10);
                    imageView.setY(10);
                    imageView.setFitWidth(100);
                    imageView.setPreserveRatio(true);

                    Photo toAdd = storeAlbums.get(i).photoPath.get(j);
                    paths.add(toAdd);

                    obvPhotos.add(imageView);*/

                    //insert stuff into the listview

                    firstTagFound = false;
                    secondTagFound = false;
                }
            }
        }
    }

    public void or() {
        tagValue = (EditText) findViewById(R.id.tagValue);
        String t1 = tagValue.getText().toString();
        tagValue2 = (EditText) findViewById(R.id.tageValue2);
        String t2 = tagValue2.getText().toString();

        boolean foundAlready = false;
        for(int i = 0; i < albums.size(); i++) { //go through list of albums
            for(int j = 0; j < albums.get(i).photoPath.size(); j++) { //go through list of photos in album i
                for(int k = 0; k < albums.get(i).photoPath.get(j).tags.size(); k++) { //go through list of tags in photo j of album i
                    if((albums.get(i).photoPath.get(j).tags.get(k).getTagType().equals(item1)
                            && albums.get(i).photoPath.get(j).tags.get(k).getTagValue().contains(t1))
                            || (albums.get(i).photoPath.get(j).tags.get(k).getTagType().equals(item2)
                            && albums.get(i).photoPath.get(j).tags.get(k).getTagValue().contains(t2)) && !foundAlready) {
                        foundAlready = true;

                        Uri imageUri = albums.get(i).photoPath.get(j).getUri();
                        imgAdapter.add(imageUri);
                        gridView.setAdapter(imgAdapter);
                        imgAdapter.notifyDataSetChanged();
                       /* InputStream stream = new FileInputStream(storeAlbums.get(i).photoPath.get(j).getPath());
                        Image list = new Image(stream);
                        ImageView imageView = new ImageView();
                        imageView.setImage(list);
                        imageView.setX(10);
                        imageView.setY(10);
                        imageView.setFitWidth(100);
                        imageView.setPreserveRatio(true);

                        Photo toAdd = storeAlbums.get(i).photoPath.get(j);
                        paths.add(toAdd);

                        obvPhotos.add(imageView);*/
                        //stuff to add to listview
                    }
                }
                foundAlready = false;
            }
        }
    }


}