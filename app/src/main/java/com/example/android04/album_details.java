package com.example.android04;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import java.util.ArrayList;

public class album_details extends AppCompatActivity {

    private static final int IMAGE_PICK_CODE = 1000;
    public static final String number = "number";

    Button addPhoto, openDisplay, deletePhoto, movePhoto;

    //ListView photoList;

    //ImageView testing, imageView, test;

    EditText albumName, positionGrid;

    ArrayList<String> obvPhotos = new ArrayList<String>();

    ArrayAdapter<String> adapter;

    static Album album;

    static int index = -1;

   // Photo addToList = new Photo();

    public static MyAdapter imgAdapter;
    public static GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle data = getIntent().getExtras();
        album = (Album) data.getParcelable("album");

        //photoList  = findViewById(R.id.photoList);
        addPhoto = findViewById(R.id.addPhoto);
        openDisplay = findViewById(R.id.openDisplay);
        deletePhoto = findViewById(R.id.deletePhoto);
        movePhoto = findViewById(R.id.movePhoto);
        //test = findViewById(R.id.test);
        //photoList = (ListView)findViewById(R.id.photoList);

        gridView = findViewById(R.id.hello);
        imgAdapter = new MyAdapter(this);
        final GridView gridview = (GridView) findViewById(R.id.hello);
        gridview.setAdapter(imgAdapter);

        /*adapter = new ArrayAdapter<String>(album_details.this, android.R.layout.simple_list_item_1, obvPhotos);
        photoList.setAdapter(adapter);*/

        //handle add photo button click
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery();
            }
        });


        openDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                Intent intent = new Intent(getApplicationContext(), photo_display.class);
                startActivity(intent);
            }
        });

        deletePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                deletePhoto(imgAdapter);
            }
        });

        movePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                movePhoto(imgAdapter);
            }
        });

        //gridView.setOnItemClickListener((parent, view, position, id) -> showPhoto(position));
        //photoList.setOnItemClickListener((parent, view, position, id) -> showPhoto(position));
    }

    private void showPhoto(int pos) {
        //Bundle bundle = new Bundle();
        //bundle.putString(number, obvPhotos.get(pos));
        //bundle.putParcelable("Photo", obvPhotos.get(pos));
        //intent.putExtra("MyClass", obj);
        //Intent intent = new Intent(this, photo_display.class);
        //intent.putExtra("photo", album.photoPath.get(0));
        //startActivity(intent);
        /*Bundle bundle = new Bundle();
        bundle.putString(number, obvPhotos.get(pos));
        Intent intent = new Intent(this, photo_display.class);
        intent.putExtras(bundle);
        startActivity(intent);*/

        Bundle bundle = new Bundle();
        bundle.putParcelable("album", album);
        Intent intent = new Intent(this, photo_display.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void pickImageFromGallery() {
        //intent to pick image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    //handle result of picked image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            //set the test imageview on the bottom of the screen using the intent data
            //test.setImageURI(data.getData());

            index++;
            Photo picture = new Photo(data.getData());
            Uri imageUri = data.getData();

            imgAdapter.add(imageUri);
            gridView.setAdapter(imgAdapter);
            album.photoPath.add(picture);
            imgAdapter.notifyDataSetChanged();


            //put photo in the list view and add the intent to the arraylist of intents in the photo class
            /*album.photoPath.add(addToList);//add photo to arraylist of Photos for this album
            album.addedPhoto();
            album.photoPath.get(album.photoPath.size()-1).setImageIntent(data); // set the intent of the image

            String newText = String.valueOf(album.photos);
            obvPhotos.add(newText);
            adapter.notifyDataSetChanged();
            */
            /*  -------------------- IGNORE STUFF AFTER HERE (working on custom adapter) ----------------------*/

            //make a new imageview to put in the listview
            //ImageView newImage = new ImageView(this);
            //newImage.setImageURI(data.getData());
            //obvPhotos.add(newImage);
            //adapter.notifyDataSetChanged();

            //setting up the adapter for the listview
            //MyAdapter adapter = new MyAdapter(album_details.this, data);
            //photoList.setAdapter(adapter);
            //System.out.println(album.photos);
            //System.out.println(album.photoPath.get(album.photos-1));


        }
    }


    public void deletePhoto(MyAdapter adapter) {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deletePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imgAdapter.remove(position);
                        gridView.setAdapter(imgAdapter);
                        album.photoPath.remove(position);
                        imgAdapter.notifyDataSetChanged();
                    }
                });

            }
        });
    }


    public void movePhoto(MyAdapter adapter) {
        albumName = (EditText)findViewById(R.id.albumName);
        String alName = albumName.getText().toString();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                movePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imgAdapter.remove(position);
                        gridView.setAdapter(imgAdapter);
                        Uri picUri = album.photoPath.get(0).getUri();//get index of selected photo?
                        album.photoPath.remove(position);
                        imgAdapter.notifyDataSetChanged();
                        for(int i = 0; i < MainActivity.obvAlbums.size()-1; i++){
                            if(alName.equals(MainActivity.obvAlbums.get(i).getName())){
                                Photo pic = new Photo(picUri);
                                MainActivity.obvAlbums.get(i).addPath(pic);
                            }
                        }
                    }
                });

            }
        });
    }
}