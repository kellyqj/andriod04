package com.example.android04;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;



public class Album implements Serializable, Parcelable {

    private static final long serialVersionUID = 3731056854131041153L;


    String name;

    int photos;

    ArrayList<Photo> photoPath = new ArrayList<Photo>();

    public Album() {
        name = "";
        photos = 0;
    }

    public Album(String albumName) {
        name = albumName;
        photos = 0; //amount of photos is automatically 0
    }

    protected Album(Parcel in) {
        name = in.readString();
        photos = in.readInt();
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel in) {
            return new Album(in);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };

    public void addedPhoto() {
        photos += 1;
    }

    public void deletedPhoto() {
        photos -= 1;
    }

    public String getName() {
        return name;
    }

    public int getPhotoAmount() {
        return photos;
    }

    public void setName(String albumName) {
        name = albumName;
    }

    public void addPath(Photo path) {
        photoPath.add(path);
    }

    public String toString() {
        if(photoPath.size() >= 1) {

            return "Name: " + name + ", Photo amount: " + photoPath.size();
        }
        return "Name: " + name + ", Photo amount: " + photoPath.size();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(photos);
    }
}
