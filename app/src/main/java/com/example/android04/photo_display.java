package com.example.android04;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class photo_display extends AppCompatActivity {

    Photo photo;

    EditText tagValueList;

    ListView tagList;

    ArrayList<Tag> tags = new ArrayList<Tag>();

    ArrayAdapter<Tag> adapter;

    Button addTag, deleteTag;

    String item;

    static Album album = album_details.album;

    Photo pic;
    static int index;

    ImageView picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_display);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        index = album_details.index;
        picture = (ImageView)findViewById(R.id.picture);

        //Bundle data = getIntent().getExtras();
        //album = (Album) data.getParcelable("album");
        //Bundle data = getIntent().getExtras();
        //Intent i = getIntent();
        //pic = (Photo)i.getSerializableExtra("photo");
        //getIntent().getSerializableExtra("photo");
        picture.setImageURI(album.photoPath.get(index).getUri());

        //setting up the spinner
        Spinner spinner = (Spinner)findViewById(R.id.tagType);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                item = parent.getItemAtPosition(position).toString();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> tagTypes = new ArrayList<String>();
        tagTypes.add("person");
        tagTypes.add("location");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(photo_display.this, android.R.layout.simple_spinner_item, tagTypes);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapter);

        //setting up tag listview
        tagList = (ListView)findViewById(R.id.tagList);
        adapter = new ArrayAdapter<Tag>(photo_display.this, android.R.layout.simple_list_item_1, tags);
        tagList.setAdapter(adapter);

        addTag = (Button)findViewById(R.id.addTag);
        addTag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addTag(adapter);
            }

        });

        deleteTag = (Button)findViewById(R.id.deleteTag);
        deleteTag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteTag(adapter);
            }

        });
    }
    //need a load tags? or will it just come up

    public void addTag(ArrayAdapter<Tag> adapter) {
        tagValueList = (EditText) findViewById(R.id.tagValueList);
        String tagVal = tagValueList.getText().toString();
        if(tagVal.trim().equals("")) {
            new AlertDialog.Builder(photo_display.this)
                    .setTitle("Empty tag value")
                    .setMessage("There is no tag value entered")
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }

        Tag addTag = new Tag();
        addTag.setTagType(item);
        addTag.setTagValue(tagVal.trim());

        for(int i = 0; i < tags.size(); i++) {
            Tag check = tags.get(i); //gets the tag information
            if (item.equals(check.getTagType()) && tagVal.trim().equals(check.getTagValue())) {
                new AlertDialog.Builder(photo_display.this)
                        .setTitle("Tag type and value already exist")
                        .setMessage("This tag type and value pair already exist!")
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return;
            }
        }

        new AlertDialog.Builder(photo_display.this)
                .setTitle("Add Tag")
                .setMessage("Are you sure you want to add this tag?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        album.photoPath.get(index).addTag(addTag);
                        tags.add(addTag);
                        adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void deleteTag(ArrayAdapter<Tag> adapter) {
        tagValueList = (EditText) findViewById(R.id.tagValueList);
        String tagVal = tagValueList.getText().toString();
        new AlertDialog.Builder(photo_display.this)
                .setTitle("Delete Tag")
                .setMessage("Are you sure you want to delete this tag?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean found = false;
                        int in = 0;
                        for(int i = 0; i < tags.size(); i++) { //loops into obvSongs to check if the album exists or not
                            if(tags.get(i).getTagType().equalsIgnoreCase(item) && tags.get(i).getTagValue().equalsIgnoreCase(tagVal)) { //obvAlbum name = delete name
                                found = true;
                                in = i;
                                break;
                            }
                        }
                        if(found) {
                            tags.remove(in);
                            adapter.notifyDataSetChanged();
                        } else { //found is false
                            new AlertDialog.Builder(photo_display.this)
                                    .setTitle("Album not found")
                                    .setMessage("Album cannot be found!")
                                    .setNegativeButton(android.R.string.no, null)
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                            return;
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();


    }

        /*Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Delete Confirmation");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to delete this tag?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Tag deleteTag = tags.getSelectionModel().getSelectedItem();
            int selectIndex = obvTags.indexOf(deleteTag);
            obvTags.remove(selectIndex);
            pic.tags.remove(selectIndex);
        }*/

}