package com.example.android04;

import java.io.Serializable;

public class Tag implements Serializable{

    private static final long serialVersionUID = 3731056854131041153L;

    String tagType;

    String tagValue;

    public Tag() {
        tagType = "";
        tagValue = "";
    }

    public Tag(String type, String value) {
        tagType = type;
        tagValue = value;
    }

    public String getTagType() {
        return tagType;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagType(String type) {
        tagType = type;
    }

    public void setTagValue(String value) {
        tagValue = value;
    }

    public String toString() {
        return "(" + tagType + "," + tagValue + ")";
    }
}
